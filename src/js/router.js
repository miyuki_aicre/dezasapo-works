/*---------------------------------------*/
/* lib */
/*---------------------------------------*/
import Vue from 'vue';
import VueRouter from 'vue-router';


/*---------------------------------------*/
/* components */
/*---------------------------------------*/
import Home from '../vue/home.vue';


/*---------------------------------------*/
/* ルーターセット */
/*---------------------------------------*/
Vue.use(VueRouter);


/* ルーター定義 */
/*---------------------------------------*/
const routes = [
	{path: '/', name: 'home', component: Home},
];


export default new VueRouter({
	routes,
})
