<?php 
/**
 * 投稿タイプ(post)の管理画面での表示名をを変更
 *
 **/
function change_post_type_labels(){
	global $menu;
	global $submenu;
	$menu[5][0] = 'お知らせ';
	$submenu['edit.php'][5][0] = 'お知らせ一覧';
	$submenu['edit.php'][10][0] = '新規追加';
}

function change_post_object_labels(){
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'お知らせ';
	$labels->singular_name = 'お知らせ';
	$labels->name_admin_bar = 'お知らせ';
	$labels->add_new = '新規追加';
	$labels->add_new_item = 'お知らせを追加';
	$labels->edit_item = 'お知らせの編集';
	$labels->new_item = '新規お知らせ';
	$labels->view_item = 'お知らせを表示';
	$labels->search_items = 'お知らせを検索';
	$labels->not_found = 'お知らせが見つかりませんでした';
	$labels->not_found_in_trash = 'ゴミ箱にお知らせは見つかりませんでした';
}
// add_action('init', 'change_post_object_labels');
// add_action('admin_menu', 'change_post_type_labels');



/**
 * ACFのgoogle map を有効化
 * 開発環境でON
 **/
function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDBozdCu6MUEeZOKXS2y5kza3W9OdJJV2U');
}
// add_action('acf/init', 'my_acf_init');



/**
 * ACFのフィールドを保存
 * @doc https://www.advancedcustomfields.com/resources/local-json/
 * @param string $path
 * @return string
 * @author 
 **/
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
    $path = get_stylesheet_directory() . '/acfopt';
    return $path;
}


/**
 * ACFのフィールドを読み込み
 * @doc https://www.advancedcustomfields.com/resources/local-json/
 * @param array $paths
 * @return string
 * @author 
 **/
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acfopt';
    return $paths;
}


